<?php

/**
 * Salinardi_Helloworld
 */

/**
 * class Salinardi_Helloworld_Helper_Data
 *
 * Main Helper.
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Salinardi_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}