<?php

/**
 * Salinardi_Helloworld
 */

/**
 * class Salinardi_Helloworld_Block_Helloworld
 *
 * Main Helper.
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Salinardi_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{
    /**
     * isEnabled
     *
     * Retuns true if the module is enable
     * @return boolean
     */
    public function isEnebled()
    {
        return Mage::helper('salinardi_helloworld')->isEnabled();
    }

    /**
     * getMessage
     *
     * returns the custom message, if the module is enabled.
     * @return string|boolean
     */
    public function getMessage()
    {
        if ($this->isEnebled()) {
            return Mage::helper('salinardi_helloworld')->getConfigData('configuration/custom_message');
        }
        return false;
    }

    public function getHelloworldUrl()
    {
        return Mage::getBaseUrl().'helloworld';
    }
}